# fastphase

Python implementation of the Scheet and Stephens model and extensions.

If you use this package you must cite the original paper:

Paul Scheet and Matthew Stephens (2006) *A Fast and Flexible Statistical Model for Large-Scale Population Genotype Data: Applications to Inferring Missing Genotypes and Haplotypic Phase* The American Journal of Human Genetics 78(4):629-644
[DOI](https://doi.org/10.1086/502802).
